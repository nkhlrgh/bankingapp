REM   Script: handsOneSQL
REM   Hands on on SQL

create table customer (  
    customer_id int primary key,  
    name varchar(50),  
    dob date,  
    gender varchar(50) check (gender in ('male','female','others'),  
    city varchar(50)  
);

create table customer (  
    customer_id int primary key,  
    name varchar(50),  
    dob date,  
    gender varchar(50) check (gender in ('male','female','others')),  
    city varchar(50)  
);

create table transaction (  
    transaction_id int primary key,  
    transaction_type varchar(10) check (transaction_type in ('credit', 'debit')),  
    transaction_amt int check (transaction_amt > 1 and transaction_amt < 10000),  
    transaction_date_time datetime,  
    transaction_from int,  
    transaction_to int,  
    transaction_status varchar(15) check (transaction_status in ('new', 'in progress', 'processed', 'pending', 'failed', 'retry')), 
    constraint fk_from foreign key (transaction_from) references customer(customer_id), 
    constraint fk_to foreign key (transaction_to) references customer(customer_id) 
);

create table transaction (  
    transaction_id int primary key,  
    transaction_type varchar(10) check (transaction_type in ('credit', 'debit')),  
    transaction_amt int check (transaction_amt > 1 and transaction_amt < 10000),  
    transaction_date_time timestamp default current_timestamp,  
    transaction_from int,  
    transaction_to int,  
    transaction_status varchar(15) check (transaction_status in ('new', 'in progress', 'processed', 'pending', 'failed', 'retry')), 
    constraint fk_from foreign key (transaction_from) references customer(customer_id), 
    constraint fk_to foreign key (transaction_to) references customer(customer_id) 
);

desc customer


desc transaction


insert into transactions ( 
    transaction_id,  
    transaction_type,  
    transaction_amt,  
    transaction_from,  
    transaction_to,  
    transaction_status 
    ) values ( 
        501,  
        'credit',  
        100,   
        5001,  
        5008,  
        'new');

insert into transaction ( 
    transaction_id,  
    transaction_type,  
    transaction_amt,  
    transaction_from,  
    transaction_to,  
    transaction_status 
    ) values ( 
        501,  
        'credit',  
        100,   
        5001,  
        5008,  
        'new');

insert into customer values ( 
    5001,  
    'Nikhil',  
    '1998-Feb-11',  
    'Male',  
    'Bangalore' 
    );

insert into customer values ( 
    5001,  
    'Nikhil',  
    '11-Feb-1998',  
    'Male',  
    'Bangalore' 
    );

insert into customer values ( 
    5001,  
    'Nikhil',  
    '1998-Feb-11',  
    'Male',  
    'Bangalore' 
    );

insert into customer values ( 
    5001,  
    'Nikhil',  
    '1998-02-11',  
    'Male',  
    'Bangalore' 
    );

insert into customer values ( 
    5001,  
    'Nikhil',  
    '1998-02-11',  
    'male',  
    'Bangalore' 
    );

insert into customer values ( 
    5001,  
    'Nikhil',  
    '1998-feb-11',  
    'male',  
    'Bangalore' 
    );

insert into customer values ( 
    5001,  
    'Nikhil',  
    '11-feb-98',  
    'male',  
    'Bangalore' 
    );

insert into customer values ( 
    5008,  
    'Nivedh',  
    '20-apr-07',  
    'male',  
    'Bangalore' 
    );

select * from customer;

insert into transaction ( 
    transaction_id,  
    transaction_type,  
    transaction_amt,  
    transaction_from,  
    transaction_to,  
    transaction_status 
    ) values ( 
        501,  
        'credit',  
        100,   
        5001,  
        5008,  
        'new');

insert into transaction ( 
    transaction_id,  
    transaction_type,  
    transaction_amt,  
    transaction_from,  
    transaction_to,  
    transaction_status 
    ) values ( 
        501,  
        'credit',  
        100,   
        5001,  
        5008,  
        'new');

select * from transaction;

insert into customer values (5002, 'Suraj', '05-oct-97', 'Male', 'Bangalore');

insert into customer values (5003, 'Vishal', '20-march-96', 'Male', 'Bangalore');

insert into customer values (5002, 'Suraj', '05-oct-97', 'male', 'Bangalore');

insert into customer values (5003, 'Vishal', '20-march-96', 'male', 'Bangalore');

insert into customer values (5004, 'Sharath', '01-may-86', 'male', 'Chennai');

insert into customer values (5005, 'Rohit', '17-jul-96', 'male', 'Bangalore');

insert into customer values (5006, 'Abhinanda', '02-sep-98', 'female', 'Kochi');

insert into customer values (5007, 'Indira', '27-nov-97', 'female', 'Salem');

select * from customer;

insert into transactions values (502, 'debit', 2000, 5002, 5002, 'processed');

insert into transaction values (502, 'debit', 2000, 5002, 5002, 'processed');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (502, 'debit', 2000, 5002, 5002, 'processed');

select * from transaction;

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (503, 'debit', 500, 5004, 5004, 'failed');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (504, 'debit', 5000, 5006, 5006, 'new');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (505, 'credit', 34, 5005, 5002, 'pending');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (506, 'debit', 50, 5003, 5003, 'retry');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (507, 'credit', 450, 5004, 5006, 'in progress');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (508, 'debit', 20, 5007, 5007, 'processed');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (509, 'debit', 1, 5008, 5008, 'failed');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (510, 'credit', 7020, 5002, 5001, 'processed');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (511, 'debit', 150, 5007, 5007, 'new');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (512, 'credit', 3150, 5001, 5006, 'pending');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (513, 'debit', 45, 5005, 5005, 'retry');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (514, 'credit', 1, 5008, 5008, 'in progress');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (515, 'credit', 20, 5003, 5004, 'processed');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (516, 'debit', 250, 5001, 5001, 'retry');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (517, 'credit', 1000, 5002, 5008, 'new');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (518, 'debit', 500, 5005, 5005, 'in progress');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (519, 'credit', 10, 5004, 5007, 'failed');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (520, 'credit', 205, 5005, 5007, 'failed');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (521, 'debit', 2500, 5008, 5008, 'processed');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (522, 'credit', 100, 5008, 5001, 'new');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (523, 'debit', 510, 5001, 5001, 'in progress');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (524, 'credit', 12, 5004, 5007, 'retry');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (525, 'credit', 220, 5002, 5004, 'processed');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (526, 'debit', 250, 5006, 5006, 'retry');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (527, 'credit', 3400, 5002, 5008, 'in progress');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (528, 'debit', 500, 5003, 5003, 'processed');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (529, 'credit', 101, 5004, 5007, 'new');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (530, 'credit', 215, 5001, 5008, 'processed');

select * from transaction;

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (514, 'credit', 1, 5008, 5008, 'in progress');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (507, 'credit', 450, 5004, 5006, 'in progress');

select * from transaction;

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (514, 'credit', 1, 5008, 5004, 'in progress');

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (514, 'credit', 1, 5008, 5004, 'in progress');

select * from transaction;

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (509, 'debit', 10, 5008, 5008, 'failed');

select * from transaction;

insert into transaction (transaction_id, transaction_type, transaction_amt, transaction_from, transaction_to, transaction_status) values (514, 'credit', 15, 5008, 5004, 'in progress');

select * from transaction;

select * from customer where dob < '01-jan-05' and dob > '01-jan-91';

select * from customer where city = 'Bangalore';

select distinct city from customer;

select * from transaction where transaction_amt < 250;

select * from transaction where transaction_amt > 500 and transaction_type = 'credit';

select * from transaction where transaction_status = 'in progress';

select * from transaction where transaction_status = 'pending' or transaction_status = 'retry';

select * from transaction where transaction_status = 'new' and transaction_type = 'debit';

select * from customer cust inner join transaction transact on cust.customer_id = transact.transaction_from and cust.city = 'Chennai' and transact.transaction_type = 'debit';

select * from customer cust inner join transactions transact on cust.customer_id = transact.transaction_from and transact.transaction_status = 'failed' and cust.dob < '01-jan-89';

select * from customer cust inner join transaction transact on cust.customer_id = transact.transaction_from and transact.transaction_status = 'failed' and cust.dob < '01-jan-89';

select * from customer cust inner join transaction transact on cust.customer_id = transact.transaction_from and cust.gender = 'female' and transact.transaction_status in ('new', 'retry');

